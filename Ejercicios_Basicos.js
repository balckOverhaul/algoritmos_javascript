//* 1)Leer un número entero y determinar si es negativo.
var numero = -4
if (numero < 0) {
    console.log("numero negativo.")
}else{
    console.log("numero positivo")
}

//* 2)Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.

numero = 42
div = numero / 10
digito1 = parseInt(div)
res = numero % 10
digito2 = parseInt(res)

if(digito1 % 2 == 0){
    
    if (digito2%2 == 0) {
        console.log(`${digito1} es par y  ${digito2} tambien.`)
    }else{
        console.log(`${digito1} es par y  ${digito2} es impar.`)
    }
}else{
    if (digito2/2 == 0) {
        console.log(`${digito1} es impar y  ${digito2} es par.`)
    }else{
        console.log(`${digito1} es impar y  ${digito2} tambien.`)
    }
}

//* 4)Leer un número entero de dos digitos menor que 20 y determinar si es primo.

numero = 17
let c = 0
for (let i = 1; i <=numero; i++) {
    if(numero % i == 0){
        if (i == 1 || i == numero) {
            c -= 1   
        } else {
            c +=1
        }
    }
}
if(c == -2){
    console.log(`${numero} es un numero primo.`)
}else{
    console.log(`${numero} no es un numero primo.`)
}

//* 5)Leer un número entero de dos digitos y determinar si es primo y además si es negativo.

numero = -17
let c = 0
for (let i = 1; i <=numero; i++) {
    if(numero % i == 0){
        if (i == 1 || i == numero) {
            c -= 1   
        } else {
            c +=1
        }
    }
}
if(c == -2){
    if (numero < 0) {
        console.log(`${numero} es un numero primo negativo.`)
    } else {
        console.log(`${numero} es un numero primo positivo.`)
    }
    
}else{
    if (numero < 0) {
        console.log(`${numero} no es un numero primo pero es negativo.`)
    } else {
        console.log(`${numero} no es un numero primo pero es positivo.`)
    }
    
}

//* 6)Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.
var numero = 29
var div = numero / 10
var digito1 = parseInt(div)
var res = numero % 10
var digito2 = parseInt(res)
var cod = false
for (let i = 1; i <10; i++) {
    if (digito1*i == digito2) {
        console.log(`${digito1} es multiplo de ${digito2}.`)
        cod = true        
    }
    if(digito2*i == digito1){
        console.log(`${digito2} es multiplo de ${digito1}.`)
        cod = true
    }
}
if(cod == false){
    console.log(`ni ${digito2} ni ${digito1} son multiplos uno del otro.`)
}

//* 7)Leer un número entero de dos digitos y determinar si los dos digitos son iguales.
var numero = 42
var div = numero / 10
var digito1 = parseInt(div)
var res = numero % 10
var digito2 = parseInt(res)

if(digito1 == digito2){
    console.log(`${digito1} y ${digito2} son iguales.`)
}else{
    console.log(`${digito1} y ${digito2} son diferentes.`)
}

//*8)Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo.
var numero = 4224
var div = numero / 1000
var digito1 = parseInt(div)
var digito4 = parseInt(numero%10)
div = parseInt(numero/10)
var digito3 = parseInt(div%10)
div = parseInt(numero/100)
var digito2 = parseInt(div%10)

if(digito3 == digito2){
    console.log(`${digito3} y ${digito2} son iguales.`)
}else{
    console.log(`${digito2} y ${digito3} son diferentes.`)
}

//* 9)Leer tres numeros enteros y determinar si el último digito de los tres numeros es igual.

var numero1 = 69
var numero2 = 29
var numero3 = 59

let digi1=numero1%10
let digi2=numero2%10
let digi3=numero3%10

if (digi1 == digi2 && digi2 == digi3) {
    console.log("Son iguales")
}else{
    console.log("Son diferentes")
}

//* 10)Leer dos numeros enteros y determinar si la diferencia entre los dos es un número divisor exacto de alguno de los dos números.

var numero1 = 4
var numero2 = 2
var divi1 = numero1-numero2
var divi2 = numero2-numero1

if(numero1%divi1 == 0){
    console.log(`${numero1}-${numero2} = ${divi1}, es divisor exacto de ${numero1}`)
    if (numero2%divi1 == 0) {
        console.log(`${numero1}-${numero2} = ${divi1}, es divisor exacto de ${numero2}`)
    } else {
        console.log(`${numero1}-${numero2} = ${divi1}, no es divisor exacto de ${numero2}`)
    }

}else{
    console.log(`${numero1}-${numero2} = ${divi1}, no es divisor exacto de ${numero1}`)
    if (numero2%divi1 == 0) {
        console.log(`${numero1}-${numero2} = ${divi1}, es divisor exacto de ${numero2}`)
    } else {
        console.log(`${numero1}-${numero2} = ${divi1}, no es divisor exacto de ${numero2}`)
    }
}

if(numero1%divi2 == 0){
    console.log(`${numero2}-${numero1} = ${divi2}, es divisor exacto de ${numero1}`)
    if (numero2%divi2 == 0) {
        console.log(`${numero2}-${numero1} = ${divi2}, es divisor exacto de ${numero2}`)
    } else {
        console.log(`${numero2}-${numero1} = ${divi2}, no es divisor exacto de ${numero2}`)
    }

}else{
    console.log(`${numero2}-${numero1} = ${divi2}, no es divisor exacto de ${numero1}`)
    if (numero2%divi2 == 0) {
        console.log(`${numero2}-${numero1} = ${divi2}, es divisor exacto de ${numero2}`)
    } else {
        console.log(`${numero2}-${numero1} = ${divi2}, no es divisor exacto de ${numero2}`)
    }
}

//* 11) Leer un número entero y determinar a cuánto es igual la suma de sus digitos.

var numero = 123456
var letras = numero.toString()
var subtotal = numero
var total = 0
var decimales = 1

for (let i = 1; i <=letras.length; i++) {
    total +=  parseInt(subtotal%10)
    decimales = decimales*10
    subtotal = parseInt(numero/(decimales))
}

console.log(`La suma de los digitos es ${total}`)

//* 12)Leer un número entero y determinar cuantas veces tiene el digito 1

var numero = 123451
var letras = numero.toString()
var subtotal = numero
var total = 0
var decimales = 1
var count = 0
for (let i = 1; i <=letras.length; i++) {
    if(parseInt(subtotal%10) == 1){
        count += 1
    }
    total +=  parseInt(subtotal%10)
    decimales = decimales*10
    subtotal = parseInt(numero/(decimales))
}

console.log(`El numero 1 se repite ${count} veces.`)

//* 13)Generar todas las tablas de multiplicar del 1 al 10

for(let i = 1;i<=10;i++){
    for(let y = 1;y<=10;y++){
        console.log(`${i}x${y} = ${i*y} `)
    }
    console.log("\n")
}

//* 14)Leer un número entero y mostrar en pantalla su tabla de multiplicar

numero = 3
for(let i = 1;i<=10;i++){
    console.log(`${i}x${numero} = ${i*numero} `)
}

//* 15)Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor.
let vector = [12,35,60,89,1,99,76,43,56,34,99,70,87,43,90,43,34]
menor = null
for(let i = 0;i<vector.length;i++){
    if(menor == null){
        menor = vector[i]
    }else{
        if(vector[i]>menor){
            menor = vector[i]
        }
    }
}
console.log(`El numero mayor es ${menor}`)

//* 16)Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentran los números terminados en O

count = 0
for(let i = 0;i<vector.length;i++){
    if(vector[i]%10 < 1){
        count += 1
    }
}
console.log(`Los numeros terminados en 0 son ${count}`)

//* 17)Leer 10 números enteros, almacenados en un vcetor y determinar cuántas veces está repetido el mayor
count = 0
for(let i = 0;i<vector.length;i++){
    if(menor == null){
        menor = vector[i]
    }else{
        if(vector[i]>=menor){
            menor = vector[i]
            count += 1
        }
    }
}
console.log(`El numero ${menor} se repite ${count} veces.`)
