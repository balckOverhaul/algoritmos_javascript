//*Leer un número entena de dos digitos y determinar si ambos digitos son pares.

numero = 42
div = numero / 10
digito1 = parseInt(div)
res = numero % 10
digito2 = parseInt(res)

if(digito1 % 2 == 1){
    console.log(`El numero ${digito1} es impar`)
}else{
    console.log(`El numero ${digito1} es par`)
}

if(digito2 % 2 == 1){
    console.log(`El numero ${digito2} es impar`)
}else{
    console.log(`El numero ${digito2} es par`)
}